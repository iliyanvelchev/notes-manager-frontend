import React from 'react';

class Field extends React.Component {
    constructor(props) {
        super(props)
        this.type = props.type
        this.label = props.label
    }

    onInputChange = (event) => {
        this.props.onInputChange(event.target.value)
    }

    render() {
        return (
            <div className="field">
                <label>{this.label}</label>
                <input type={`${this.type}`} onChange={this.onInputChange} />
            </div>
        );
    }
}

export default Field;