import React from 'react'

class SubmitButton extends React.Component {
    constructor(props) {
        super(props)
        this.label = props.label
        this.className = this.props.className
    }
    render() {
        return (
            <button className={this.className} type='submit'>{this.label}</button>
        )
    }
}

export default SubmitButton