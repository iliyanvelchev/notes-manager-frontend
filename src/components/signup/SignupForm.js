import './SignupForm.css'
import React from 'react'
import Field from '../common/Field'
import SubmitButton from '../common/SubmitButton'
import users from '../../api/users'
import { Redirect, withRouter } from 'react-router-dom'

class SignupForm extends React.Component {
    state = { name: '', email: '', password: '', age: null }

    signup = async (event) => {
        event.preventDefault()
        const response = await users.post('/', {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            age: this.state.age
        })

        if (response.status === 201) {
            this.setState({ name: '', email: '', password: '', age: null })
            this.props.history.push('/');
        }
    }

    onNameInputChange = (name) => {
        this.setState({ name: name });
    }

    onEmailInputChange = (email) => {
        this.setState({ email: email });
    }

    onPasswordInputChange = (password) => {
        this.setState({ password: password });
    }

    onAgeInputChange = (age) => {
        this.setState({ age: age });
    }

    render() {
        if (!localStorage.getItem('token')) {
            return (
                <div>
                    <div className="ui container">
                        <h1>Sign up</h1>
                        <form onSubmit={this.signup} className="ui form">
                            <Field label='Name ' type='text' onInputChange={this.onNameInputChange} />
                            <Field label='Email ' type='text' onInputChange={this.onEmailInputChange} />
                            <Field label='Password ' type='password' onInputChange={this.onPasswordInputChange} />
                            <Field label='Age ' type='number' onInputChange={this.onAgeInputChange} />
                            <SubmitButton label='Sign up' />
                        </form>
                    </div>
                </div>
            )
        }
        else {
            return (
                <Redirect to="/" />
            )
        }
    }
}

export default withRouter(SignupForm)