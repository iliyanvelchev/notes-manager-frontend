import React from 'react'
import Note from './Note.js'

class NotesList extends React.Component {

    render() {
        const elements = this.props.notes.map((note) => {
            return <Note key={note._id} noteId={note._id} payload={note.payload} onDeleteNote={this.props.onDeleteNote} />
        })
        return <div>{elements}</div>
    }
}

export default NotesList;