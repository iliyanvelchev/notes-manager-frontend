import './Note.css'
import React from 'react'

class Note extends React.Component {

    onDelete = async (event) => {
        event.preventDefault()
        this.props.onDeleteNote(this.props.noteId)
    }

    render() {
        return (
            <div>
                <div className="info">
                    <strong>{this.props.payload}</strong>
                    <button className="remove-btn" onClick={this.onDelete}><i className="trash icon"></i>Delete</button>
                </div>
            </div>
        )
    }
}

export default Note;