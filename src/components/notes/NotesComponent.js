import './NotesComponent.css'
import React from 'react'
import NotesList from './NotesList'
import users from '../../api/users'
import notes from '../../api/notes'
import SubmitButton from '../common/SubmitButton'
import { Redirect } from 'react-router-dom'
class NotesComponent extends React.Component {
    state = { notes: [], newNote: '' }

    componentDidMount() {
        this.loadNotes()
    }

    logout = async () => {
        const headers = { 'Authorization': localStorage.getItem('token') }
        users.post('/logout', null, { headers: headers })
        localStorage.removeItem('token')
        this.setState({ notes: [] })
    }

    loadNotes = async () => {
        try {
            const headers = { 'Authorization': localStorage.getItem('token') }
            const response = await notes.get('/all', { headers: headers })
            this.setState({ notes: response.data })
        }
        catch (error) {
            if (error.response.status === 401) {
                localStorage.removeItem('token')
                this.setState({ notes: [] })
            }
        }
    }

    onDeleteNote = async (key) => {
        const headers = { 'Authorization': localStorage.getItem('token') }
        const response = await notes.delete('/id/' + key, { headers: headers })
        if (response.status === 200) {
            this.setState({ notes: this.state.notes.filter(note => note._id !== key) })
        }
    }

    onNoteChange = (event) => {
        this.setState({ newNote: event.target.value });
    }

    onAddNote = async (event) => {
        event.preventDefault()
        const headers = { 'Authorization': localStorage.getItem('token') }
        const response = await notes.post('/', { payload: this.state.newNote }, { headers: headers })
        if (response.status === 201) {
            const notes = this.state.notes;
            notes.push(response.data)
            this.setState({ notes: notes, newNote: '' })
        }
    }

    render() {
        if (!localStorage.getItem('token')) {
            return <Redirect to="/" />
        }
        return (
            <div className='authentication'>
                <button className="logout-btn" onClick={this.logout}>Logout</button>
                <div className="greeting"><strong>Notes Manager</strong></div>
                <NotesList notes={this.state.notes} onDeleteNote={this.onDeleteNote} />
                <form onSubmit={this.onAddNote}>
                    <div className="field">
                        <textarea className="note" value={this.state.newNote} onChange={this.onNoteChange}></textarea>
                    </div>
                    <SubmitButton className="post-note-btn" label="Add Note" />
                </form>
            </div>
        )
    }
}

export default NotesComponent