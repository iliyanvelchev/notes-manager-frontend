import React from 'react';
import LoginForm from './login/LoginForm'
import SignupForm from './signup/SignupForm'
import NotesComponent from './notes/NotesComponent'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

class App extends React.Component {

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path='/' exact component={LoginForm} />
                    <Route path='/signup' component={SignupForm} />
                    <Route path='/notes' component={NotesComponent} />
                </Switch>
            </BrowserRouter>
        )
    }
}

export default App