import './LoginForm.css'
import React from 'react'
import users from '../../api/users'
import Field from '../common/Field'
import SubmitButton from '../common/SubmitButton'
import { Link, Redirect } from 'react-router-dom'
class LoginForm extends React.Component {
    state = { token: null, email: '', password: '' }

    login = async (event) => {
        event.preventDefault();
        const data = { email: this.state.email, password: this.state.password }
        const headers = { 'Authorization': localStorage.getItem('token') }

        const response = await users.post('/login', data, { headers: headers });

        const token = 'Bearer ' + response.data.token;
        localStorage.setItem('token', token)
        this.setState({ token: token, email: '', password: '' })
    }

    logout = async (event) => {
        event.preventDefault();
        const headers = { 'Authorization': localStorage.getItem('token') }

        const response = await users.post('/logout', null, { headers: headers })

        if (response.status === 200) {
            localStorage.removeItem('token')
            this.setState({ token: null })
        }
    }

    onEmailInputChange = (email) => {
        this.setState({ email: email });
    }

    onPasswordInputChange = (password) => {
        this.setState({ password: password });
    }

    render() {
        if (localStorage.getItem('token')) {
            return <Redirect to='/notes' />
        }
        return (
            <div className="ui container">
                <h1 className='login-form'>Welcome to the Notes App</h1>
                <form onSubmit={this.login} className="ui form">
                    <Field label='Email ' type='text' onInputChange={this.onEmailInputChange} />
                    <Field label='Password ' type='password' onInputChange={this.onPasswordInputChange} />
                    <SubmitButton className='login-btn' label='Login' />
                    <Link to='/signup' >
                        <li style={{ listStyle: 'none' }} to='/signup'>Don't have an account? Sign up here!</li>
                    </Link>
                </form>
            </div >
        )
    }
}

export default LoginForm